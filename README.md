[![language](https://img.shields.io/badge/language-python-blue)](https://www.python.org/)
[![codecov](https://codecov.io/gh/xin-huang/dadi-dev/branch/master/graph/badge.svg)](https://codecov.io/gh/xin-huang/dadi-dev)
[![build Status](https://travis-ci.org/xin-huang/dadi-dev.svg?branch=master)](https://travis-ci.org/xin-huang/dadi-dev)

A development version for ∂a∂i

Please refer to [∂a∂i](https://bitbucket.org/gutenkunstlab/dadi) for stable versions
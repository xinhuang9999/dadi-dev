import unittest
import numpy
import dadi


class ParameterSelectionTestCase(unittest.TestCase):
    def test_best_fit(self):
        numpy.random.seed(1398238)
        
        fs = dadi.Spectrum.from_file("./tests/test_data/test_1D.fs")
        
        demo_func = dadi.Numerics.make_anc_state_misid_func(dadi.Demographics1D.two_epoch)
        
        pts = [40,50,60]
        params=[2, 100, 0.5]
        upper_bound = [100, 3, 1]
        lower_bound = [1e-2, 0, 0]
        p_labels = ['nu', 'T', 'misid']
        
        best_params = dadi.ParameterSelection.best_fit(params, fs, "two_epoch", 
                                                       demo_func, 3, 3, pts=pts, fs_folded=False, param_labels=p_labels,
                                                       func_args=[], upper_bound=upper_bound, lower_bound=lower_bound)
        
        numpy.allclose(best_params, [3.45125289, 0.62949395, 0.15234636])
    

suite = unittest.TestLoader().loadTestsFromTestCase(ParameterSelectionTestCase)

if __name__ == '__main__':
    unittest.main()